// SERVICES SECTION
let servicesTitle = document.querySelectorAll('.services-section-menu-item');
servicesTitle.forEach(element => element.addEventListener('click',() =>{
    document.querySelector('.services-section-menu-item.active').classList.remove('active');
    element.classList.add('active');

    let currentDesc = document.querySelector(`#${element.getAttribute("id")}-text`);
    document.querySelector('.services-section-item-desk.active').classList.remove('active');
    currentDesc.classList.add('active');
}
))

// OUR WORK SECTION
let spinner = document.querySelector('.ring');
showSpinner = () => spinner.classList.add('active');
hideSpinner = () => spinner.classList.remove('active');

let loadMoreBtn = document.querySelector(".btn.load-more-button");
loadMoreBtn.addEventListener("click", () => {
    loadMoreBtn.classList.remove('active');
    showSpinner();
    setTimeout(hideSpinner, 2000);

    let invisibleImages = document.querySelectorAll('.work-section-photo-gallery-item');
    setTimeout(() => invisibleImages.forEach(element => element.classList.add('active')), 2000)
})


let workSectionBtn = document.querySelectorAll('.work-section-menu-item');
let allImages = document.querySelectorAll('.work-section-photo-gallery-item');
let allBtn = document.querySelector("all");
let firstSection = Array.from(allImages).slice(0, 12);

workSectionBtn.forEach(element => element.addEventListener('click', function(){
    let currentImages = Array.from(allImages).filter(function(elem){
        if(elem.getAttribute('id') === `${element.getAttribute('id')}-text`){
            return elem
    }})

    allImages.forEach(element => element.classList.remove('active'))
    currentImages.forEach(element =>element.classList.add("active") )
    
    if(element.getAttribute('id') === "all"){
        firstSection.forEach(element => element.classList.add('active'))
    }
}))

// FEEDBACK SECTTION

$('.big-slider').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    fade: true,
    asNavFor: '.small-slider'
  });
  $('.small-slider').slick({
    slidesToShow: 3,
    slidesToScroll: 1,
    asNavFor: '.big-slider',
    dots: true,
    centerMode: true,
    focusOnSelect: true
  });